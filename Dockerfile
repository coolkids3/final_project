FROM python:3.8

RUN pip install ansible

WORKDIR /ansible

COPY first_ansible/ /ansible

#COPY id_rsa /root/.ssh/id_rsa

#RUN chmod 600 /root/.ssh/id_rsa
RUN mkdir /root/.ssh
RUN chmod 600 /root/.ssh
#haal weg als geen nieuwe vm gemaakt voor ip list
COPY terraform/inventory.ini /ansible

# #checks for the files that are needed
# RUN ls -l /ansible/

# RUN ls -l /root/.ssh

#voor ssh login
ENV ANSIBLE_HOST_KEY_CHECKING=False

WORKDIR /ansible

CMD ["ansible-playbook", "-i", "/ansible/inventory.ini", "/ansible/playbook.yml", "-u", "azuregyan", "--private-key=/root/.ssh/id_rsa"]