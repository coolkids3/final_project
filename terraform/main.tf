provider "azurerm" {
  features {}
}

# Module for testing environment
module "testing" {
  source = "./testing"
}

# Module for acceptance environment
module "acceptance" {
  source = "./acceptance"
}

# Module for production environment
#module "production" {
#  source = "./production"
#}

output "testing_vm_ip" {
  value = module.testing.public_ip_address
}

output "acceptance_vm_ip" {
  value = module.acceptance.public_ip_address
}

#output "production_vm_ip" {
#  value = module.production.public_ip_address
#}