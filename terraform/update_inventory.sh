testing_vm_ip=$(terraform output testing_vm_ip)
acceptance_vm_ip=$(terraform output acceptance_vm_ip)
#production_vm_ip=$(terraform output production_vm_ip)

echo "[testing_vm]" > inventory.ini
echo $testing_vm_ip >> inventory.ini
echo "" >> inventory.ini

echo "[acceptance_vm]" >> inventory.ini
echo $acceptance_vm_ip >> inventory.ini
echo "" >> inventory.ini

echo "[production_vm]" >> inventory.ini
#echo $production_vm_ip >> inventory.ini
echo "172.211.23.91" >> inventory.ini
echo "" >> inventory.ini

# echo "[localvm]" >> inventory.ini
# echo localhost ansible_connection=local >> inventory.ini
